google.charts.load("current", { packages: ["timeline"] });
google.charts.setOnLoadCallback(drawChart);
var today = new Date();
var timeline_data = null;

async function getTimelineData() {
  if (timeline_data) {
    return timeline_data.projects;
  }
  // var promo = new URLSearchParams(window.location.search).get("promo");
  // if (!promo) throw "Missing promo parameter in URL";
  var response = await fetch("data.json", {
    cache: "reload",
  });
  if (!response.ok) throw "Could not get timeline data: " + response.statusText;
  timeline_data = await response.json();

  $("#title").text(`Epitech Timeline - Promotion ${timeline_data.promo}`);
  document.title = `Epitech Timeline - Promotion ${timeline_data.promo}`;
  $("#semester").text(`Semester ${timeline_data.semester}`);

  return timeline_data.projects;
}

async function createDataTable(bttf_displayed) {
  var dataTable = new google.visualization.DataTable();
  dataTable.addColumn({ type: "string", id: "Module" });
  dataTable.addColumn({ type: "string", id: "Project" });
  dataTable.addColumn({ type: "date", id: "Start" });
  dataTable.addColumn({ type: "date", id: "End" });
  var now = new Date(today.getFullYear(), today.getMonth(), today.getDate());

  var data = await getTimelineData();

  if (!bttf_displayed) {
    data = data.filter((v) => !v.bttf);
  }
  data = data.map(function (v) {
    var end_date = new Date(v.end);
    end_date.setDate(end_date.getDate() + 1);
    return [v.module, v.project, new Date(v.start), end_date];
  });

  dataTable.addRows([["Now", "Now", now, now]]);
  dataTable.addRows(data);
  return dataTable;
}

async function updateChart(chart) {
  var bttf_displayed = window.localStorage.getItem("bttf") == "true";
  var dataTable = await createDataTable(bttf_displayed);

  chart.draw(dataTable, {
    timeline: {
      colorByRowLabel: true,
    },
  });
}

async function drawChart() {
  var container = document.getElementById("timeline-container");
  var chart = new google.visualization.Timeline(container);

  try {
    await updateChart(chart);

    nowLine("timeline-container");

    google.visualization.events.addListener(chart, "onmouseover", function (
      obj
    ) {
      if (obj.row == 0) {
        $(".google-visualization-tooltip").css("display", "none");
      }
      nowLine("timeline-container");
    });

    google.visualization.events.addListener(chart, "onmouseout", function (
      obj
    ) {
      nowLine("timeline-container");
    });

    $("#bttf-button").click(async function () {
      var bttf_displayed = window.localStorage.getItem("bttf") == "true";
      window.localStorage.setItem("bttf", !bttf_displayed);
      await updateChart(chart);
    });
  } catch (e) {
    $("#timeline-container").text(e);
  }
}

function nowLine(div) {
  //get the height of the timeline div
  var height;
  $("#" + div + " rect").each(function (index) {
    var x = parseFloat($(this).attr("x"));
    var y = parseFloat($(this).attr("y"));

    if (x == 0 && y == 0) {
      height = parseFloat($(this).attr("height"));
    }
  });

  var nowWord = $("#" + div + ' text:contains("Now")');

  nowWord
    .prev()
    .first()
    .attr("height", height + "px")
    .attr("width", "1px")
    .attr("y", "0");
  // add this line to remove the display:none style on the vertical line
  $("#" + div + '  text:contains("Now")').each(function (idx, value) {
    if (idx == 0) {
      $(value).parent().find("rect").first().removeAttr("style");
    } else if (idx == 1) {
      $(value).parent().find("rect").first().attr("style", "display:none;");
    }
  });
}

var repourl = "https://gitlab.com/epi-codes/Epitech-2023-Timeline";
$(document).ready(function () {
  $.getJSON(
    "https://gitlab.com/api/v4/projects/epi-codes%2fEpitech-2023-Timeline/repository/commits",
    function (json) {
      var msg, el, date;

      $("#changelog-container").empty();

      for (var i = 0; i < json.length; i++) {
        msg = json[i].message.split("\n");
        date = moment(json[i].created_at);
        el = $(`<p class="commit">
<a href="${repourl}/commit/${json[i].id}" target="_blank" class="commit-msg">${
          msg[0]
        }</a>
<span title="${date.format(
          "dddd, MMMM Do YYYY, h:mm:ss a"
        )}" class="commit-date">${date.fromNow()}</span>
</p>`);
        if (msg.length > 1) {
          for (var j = msg.length - 1; j >= 1; j--) {
            if (msg[j].length > 0) {
              el.addClass("expanded");
              el.find("a").after(`<span class="commit-desc">${msg[j]}</span>`);
            }
          }
        }
        el.appendTo($("#changelog-container"));
      }

      if (json.length <= 0) {
        $("#changelog-container").text("No commits !? xO");
      }
    }
  ).fail(function () {
    $("#changelog-container").text("Error while loading changelog :'(");
  });

  function set_theme(dark) {
    var dark = dark || false;

    window.localStorage.setItem("dark", dark);

    if (dark) {
      $("body").addClass("dark");
      $("#switch").text("Switch to light");
    } else {
      $("body").removeClass("dark");
      $("#switch").text("Switch to dark");
    }
  }

  $("#switch").on("click", function () {
    set_theme(!$("body").hasClass("dark"));
    return false;
  });

  set_theme(window.localStorage.getItem("dark") == "true" ? true : false);
  setTimeout(function () {
    $("body").addClass("ready");
  }, 500);
});
